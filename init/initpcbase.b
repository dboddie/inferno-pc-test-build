implement InitShell;

include "sh.m";
include "sys.m";
include "draw.m";

sys: Sys;

InitShell: module
{
    init:    fn(nil: ref Draw->Context, nil: list of string);
};

mkbind(dev, dir, target: string, flag: int)
{
    sys->bind(dev, target, flag);
    target = dir + "/" + target;
    sys->create(target, Sys->OREAD, Sys->DMDIR + 8r777);
    sys->bind(dev, target, flag);
}

init(nil: ref Draw->Context, nil: list of string)
{
    sh := load Sh Sh->PATH;
    sys = load Sys Sys->PATH;

    if(sys != nil)
        sys->print("init: starting shell\n");

    sys->bind("#S", "/dev", sys->MREPL);   # Disks
    sh->system(nil, "mount -c {mntgen} /n");
    sh->system(nil, "mount -c {mntgen} /n/local");
    sh->system(nil, "mount -c {mntgen} /n/remote");

    sys->bind("#k", "/dev", sys->MAFTER);   # devds
    sh->system(nil, "echo 'part fs /dev/sdC0/data 8388608 58720256' > /dev/ds/ctl");
    sh->system(nil, "mount -c {disk/kfs -n main /dev/ds/fs} /n/local");

    sys->bind("/n/local", "/tmp", sys->MREPL | sys->MCREATE);
    sys->create("/tmp/chan", Sys->OREAD, Sys->DMDIR + 8r777);
    sys->create("/tmp/svc", Sys->OREAD, Sys->DMDIR + 8r777);
    mkbind("#l", "/tmp", "/net", sys->MAFTER);   # Network interfaces
    mkbind("#I", "/tmp", "/net", sys->MAFTER);   # IP
    mkbind("#p", "/tmp", "/prog", sys->MREPL);   # prog device
    mkbind("#i", "/tmp", "/dev", sys->MAFTER);   # draw device
    mkbind("#m", "/tmp", "/dev", sys->MAFTER);   # pointer/mouse
    mkbind("#c", "/tmp", "/dev", sys->MAFTER);   # console device
    mkbind("#u", "/tmp", "/dev", sys->MAFTER);   # USB device
    mkbind("#t", "/tmp", "/dev", sys->MAFTER);   # serial line
    mkbind("#d", "/tmp", "/fd", Sys->MREPL);
    mkbind("#e", "/tmp", "/env", sys->MREPL|sys->MCREATE);

    sys->print("Running /init.sh\n");
    spawn sh->init(nil, "sh"::"/init.sh"::nil);
}
