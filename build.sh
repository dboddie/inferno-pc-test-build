#!/bin/bash

set -e

git clone -b 2025-01-usb-mouse https://github.com/dboddie/inferno-os.git /tmp/inferno-os

dd if=/dev/zero of=/tmp/inferno-os/9fat.part bs=1M count=8
dd if=/dev/zero of=/tmp/inferno-os/fs.part bs=1M count=56

cp -r scripts/* /tmp/inferno-os
cp config/pcbase /tmp/inferno-os/os/pc
cp init/initpcbase.b /tmp/inferno-os/os/init
cp usr/inferno/* /tmp/inferno-os/usr/inferno

pushd /tmp/inferno-os
git submodule update --init
sed -ri 's/usr\/inferno/tmp\/inferno-os/' mkconfig
sed -ri 's/SYSHOST=Plan9/SYSHOST=Linux/' mkconfig
sed -ri 's/#OBJTYPE/OBJTYPE/' mkconfig
sed -ri 's/OBJTYPE=\$/#OBJTYPE=\$/' mkconfig
sed -i s/'0, SIGKILL'/'0, SIGTERM'/ emu/Linux/os.c

./makemk.sh
export PATH=$PWD/Linux/386/bin/:$PATH
mk nuke
mk mkdirs
mk install

pushd appl/lib/usb
mk install
popd

pushd appl/cmd/usb
mk install
popd

pushd os/pc
mk CONF=pcbase
mv ipcbase ipc
gzip -9 -f ipc
cd ../boot/pc
mk mbr.install pbs.install pbslba.install 9load.install
popd

emu sh.dis /fill-9fat.sh
emu sh.dis /fill-kfs.sh

cat 9fat.part fs.part > /tmp/inferno-pc.img
popd
